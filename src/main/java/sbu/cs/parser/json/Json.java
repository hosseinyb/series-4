package sbu.cs.parser.json;

public class Json implements JsonInterface {
    private static MyHashMap hashmap = new MyHashMap();

    public void addkeyvalue (String key,String value) {
        hashmap.addKeyValue(key, value);
    }
    @Override
    public String getStringValue(String key) {
        String answer;
        answer = hashmap.getValue(key);
        return answer;
    }
}
