package sbu.cs.parser.json;

import java.util.ArrayList;


public class MyHashMap {
    private ArrayList<ArrayList<String>> hashMap = new ArrayList<ArrayList<String>>();

    public void addKeyValue(String key, String value) {
        ArrayList<String> temp = new ArrayList<String>();
        temp.add(key);
        temp.add(value);
        hashMap.add(temp);
    }
    public String getKey(String value) {
        for (int i = 0; i < hashMap.size(); i++) {
            if( hashMap.get(i).get(1).equals(value)) {
                return hashMap.get(i).get(0);
            }
        }
        return null;
    }
    public String getValue(String key) {
        for (int i = 0; i < hashMap.size(); i++) {
            if( hashMap.get(i).get(0).equals(key)) {
                return hashMap.get(i).get(1);
            }
        }
        return null;
    }
}
