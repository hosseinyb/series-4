package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonParser {

    /*
     * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        Json json = new Json();
        String[] rem = data.split("[\\{\\}]+");
        data = "";
        for (int i = 1; i < rem.length; i++) {
            data = data + rem[i];
        }
        Pattern pattern = Pattern.compile("\"([\\w\\d ]*?)\":");
        Matcher matcher = pattern.matcher(data);
        ArrayList<String> key = new ArrayList<String>();
        while (matcher.find()) {
            key.add(matcher.group());
        }
        List<String> value = Arrays.asList(data.split("\"([\\w\\d ]*?)\":"));
        for (int i = 0; i < key.size(); i++) {
            String tempValue;
            if (value.get(i +1).indexOf("[") != -1) {
                tempValue = value.get(i + 1).substring(value.get(i + 1).indexOf("["), (value.get(i + 1).lastIndexOf("]") + 1));
            } else {
                if (value.get(i +1).indexOf("\"") != -1) {
                    tempValue = value.get(i + 1).substring((value.get(i + 1).indexOf("\"") + 1), value.get(i + 1).lastIndexOf("\""));
                } else {
                    tempValue = value.get(i + 1).replaceAll("[ ,\\s]*","");
                }
            }
            json.addkeyvalue(key.get(i).replaceAll("[ \"]*", "").replaceAll(":", ""), tempValue);
        }
        return json;
    }

    /*W
     * this function is the opposite of above function. implementing this has no score and
     * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
