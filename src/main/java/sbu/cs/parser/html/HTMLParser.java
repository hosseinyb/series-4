package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLParser {

    /*
     * this function will get a String and returns a Dom object
     */
    public static Node parse(String document) {
        if (document.equals(null) || document.equals("")) {
            return null;
        }
        String name = document.substring(document.indexOf("<") + 1, document.indexOf(">"));
        String attributes = null;
        if (name.indexOf(" ") != -1) {
            attributes = name.substring(name.indexOf(" ") + 1);
            name = name.substring(0, name.indexOf(" "));
        }

        String inside;
        if (name.charAt(0) == '/') {
            inside = null;
        } else {
            inside = document.substring(document.indexOf(">") + 1, document.lastIndexOf("</" + name + ">"));
        }
        Node root = new Node(inside, name);
        root.addAttribute(extractAttributes(attributes));
        ArrayList<String> children = getChildern(inside);//------------------------------------------------
        for (String temp: children) {
            root.addChild(parse(temp));
        }
        return root;
    }

    public static HashMap<String, String> extractAttributes(String attributes) {
        if (attributes == null) {
            return null;
        }
        HashMap<String, String> attributesMap = new HashMap<>();
        Pattern pattern = Pattern.compile("([a-zA-Z]+\\s*=\\s*(\"([^\"]*)\"|'([^']*)'))*");
        Matcher matcher = pattern.matcher(attributes);
        while (matcher.find()) {
            String temp = matcher.group();
            String[] keyValue = temp.split("=");
            attributesMap.put(keyValue[0], keyValue[1].substring(1, (keyValue[1].length() - 1)));
        }
        return attributesMap;
    }

    public static ArrayList<String> getChildern(String html) {
        ArrayList<String> children = new ArrayList<>();
        while (html.contains("<")) {
            Pattern pattern = Pattern.compile("[^\\s]");
            Matcher matcher = pattern.matcher(html);
            if (!matcher.find()) {
                break;
            }
            String temp, beginigTag, endingTag;
            beginigTag = html.substring(html.indexOf("<"), html.indexOf(">") + 1);
            if (beginigTag.charAt(1) == '/') {
                children.add(beginigTag);
                continue;
            }
            if (beginigTag.indexOf(" ") != -1) {
                endingTag = "</" + beginigTag.substring(1, beginigTag.indexOf(" ")) + ">";
            } else {
                endingTag = "</" + beginigTag.substring(1);
            }
            temp = html.substring(0, html.indexOf(endingTag) + endingTag.length());
            children.add(temp);
            html = html.replaceFirst(temp, "");
        }
        return children;
    }

    /*
     * a function that will return string representation of dom object.
     * only implement this after all other functions been implemented because this
     * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
