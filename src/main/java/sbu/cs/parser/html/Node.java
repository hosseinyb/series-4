package sbu.cs.parser.html;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node implements NodeInterface {

    private String inside;
    private String name;
    HashMap<String, String> attributes = new HashMap<>();
    List<Node> children = new ArrayList<>();


    public Node(String inside, String name) {
        this.inside = inside;
        this.name = name;
    }

    public void addAttribute(HashMap<String, String> attributes) {
        this.attributes = attributes;
    }

    public void addChild(Node child) {
        children.add(child);
    }


//    {
//        Pattern pat1 = Pattern.compile("<[a-zA-Z]+(\\s+[a-zA-Z]+\\s*=\\s*(\"([^\"]*)\"|'([^']*)'))*\\s*>");
//        Pattern pat2 = Pattern.compile("<[\\w\\d]+");
//        Pattern pat3 = Pattern.compile("</[a-zA-Z]+(\\s+[a-zA-Z]+\\s*=\\s*(\"([^\"]*)\"|'([^']*)'))*\\s*>");
//        Matcher m1 = pat1.matcher(inside);
//        Matcher m3 = pat3.matcher(inside);
//        if (m1.find()) {
//            tagWithAttribiuts = m1.group();
//            Matcher m2 = pat2.matcher(tagWithAttribiuts);
//            if (m2.find()) {
//                fatherTag = m2.group();
//                fatherTag = fatherTag + ">";
//            }
//            String endOffFatherTag = "</" + fatherTag.substring(1);
//            inside = inside.replaceFirst(tagWithAttribiuts, "");
//            inside = inside.substring(0, inside.indexOf(endOffFatherTag));
//            //-------------------------------------------------------------
//        } else if (!(m1.find()) && m3.find()) {
//            tagWithAttribiuts = m3.group();
//            Matcher m2 = pat2.matcher(tagWithAttribiuts);
//            if (m2.find()) {
//                fatherTag = m2.group();
//                fatherTag = fatherTag + ">";
//            }
//        }
//        inside = inside.replaceFirst(tagWithAttribiuts, "");
//    }

    /*
     * this function will return all that exists inside a tag
     * for example for <html><body><p>hi</p></body></html>, if we are on
     * html tag this function will return <body><p1>hi</p1></body> and if we are on
     * body tag this function will return <p1>hi</p1> and if we are on
     * p tag this function will return hi
     * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        return inside;
    }

    /*
     *
     */
    @Override
    public List<Node> getChildren() {
        return children;
    }

    /*
     * in html tags all attributes are in key value shape. this function will get a attribute key
     * and return it's value as String.
     * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }
}
